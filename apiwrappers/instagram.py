import time
from libs import instagram_explore as ie


def parse_post(username_, post, *fullResult):
    text = post['caption']
    likes = post['likes']['count']
    comments = post['comments']['count']
    img_source = post['display_src']
    username = fullResult[0]['username']
    profile_pic = fullResult[0]['profile_pic_url']
    post_date = post['date']
    post_type = 'article'

    if post['is_video']:
        post_type = 'video'

    post_date = time.ctime(post_date)

    trail_post_form = {
        'author': username, 'source': 'Instagram', 'timeRaw': post_date,
        'like_': likes, 'comment_': comments, 'profilePhotoUrl': profile_pic,
        'article': str(text.encode('utf-8').strip()),
        'terms': ('comment', 'like'), 'img_source':img_source,
        'post_type': post_type, 'postNo': '', 'beforeSmileys': {},
        'afterSmileys': {}, 'bookmarkTitle':'', 'likes':[], 'comments':{},
        'commenters':{}, 'shares':[], 'headline':None,
        'created': time.ctime(), 'createdRaw': str(time.time()),
        'modified': str(time.time())}

    return trail_post_form


def get_timeline(instagram_username):
    result = None
    while not result:
        try:

            result = the_statuses = ie.user(instagram_username).data
            print result
            count_ = 0

            for status in the_statuses:
                try:
                    status['id'] = str(status['id'])
                    extracted_ = parse_post(instagram_username, status)
                    extracted_['id'] = str(status['id'])

                    the_statuses[count_] = extracted_

                except:
                    pass

                count_ += 1

            return the_statuses

        except (TypeError, ValueError):
            pass

        except KeyError:
            return None


def get_user_photos_from_timeline(instagram_timeline):
    photo_links = []
    for payload in instagram_timeline['media']['nodes']:
        photo_links.append(payload['display_src'])

    return photo_links[:6]


print get_timeline('cristiano')
