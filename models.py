import os
import random
import sys
import time
sys.path.append('lib')

from sqlalchemy import MetaData, Column, Integer, String, create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import OperationalError


def get_env(env_key):
    return os.environ.get(env_key)

if not get_env('TEST_MODE'):
    engine = create_engine(
        'mysql+mysqldb://tomisin10:MoniaDev.@tomisin10.mysql.pythonanywhere-'
        'services.com/tomisin10$default?charset=utf8&'
        'use_unicode=0', pool_recycle=3600)
else:
    engine = create_engine('sqlite:///tmp/database.db',
                           convert_unicode=True,
                           echo=False)

metadata = MetaData(bind=engine)

db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


class ModelBase(Base):
    __abstract__ = True


class Artisan(ModelBase):
    __tablename__ = 'artisans_test'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    bio = Column(String(140))
    email = Column(String(200))
    status = Column(String(100))
    skill = Column(String(100))
    phone = Column(String(100))
    instagram_handle = Column(String(100))
    street_address = Column(String(100000000))
    town = Column(String(100000000))
    state = Column(String(100000000))
    country = Column(String(100000000))
    bookings = Column(String(100000000))
    confirmation_status = Column(String(100000000))
    display_photo = Column(String(100000000))

    favourites = Column(String(100000000), default="[]")
    awards = Column(String(100000000), default="[]")
    ratings = Column(String(100000000), default="[]")

    common_xtics = ['state', 'town', 'country', 'skill']
    headers = [
        'id', 'name', 'skill', 'bio', 'phone', 'town', 'state', 'country',
        'ratings', 'bookings', 'instagram_handle', 'display_photo']

    def head(self):
        head_ = {}

        for key_ in self.headers:
            head_[key_] = getattr(self, key_)

            if key_ in ['ratings']:
                if type(head_[key_]) is str:
                    head_[key_] = eval(head_[key_])

            if key_ == 'ratings':
                try:
                    head_['ratings'] = sum(head_['ratings']) / len(
                        head_['ratings'])
                except (ZeroDivisionError, TypeError):
                    head_['ratings'] = 0

            if key_ == 'instagram_handle' and head_[key_]:
                continue

                # because of the issues with instagram
                from apiwrappers import instagram

                artisan_instagram_timeline = instagram.get_timeline(head_[key_])
                if artisan_instagram_timeline:
                    artisan_instagram_photos = (
                        instagram.get_user_photos_from_timeline(
                            artisan_instagram_timeline)
                    )

                    head_['instagram_photos'] = artisan_instagram_photos

        return head_

    def __init__(
            self, name=None, bio=None, email=None, skill=None,
            instagram_handle=None, phone=None, street_address='', town='',
            state='', country='', favourites=[], awards=[], ratings=[],
            confirmation_status='pending', spare_dict={}, display_photo=None):

        self.id = random.randint(1, 1000000000000)#self.generate_id()
        self.name = name
        self.bio = bio
        self.email = email
        self.skill = skill
        self.phone = phone
        self.instagram_handle = instagram_handle
        self.street_address = street_address
        self.town = str(town)
        self.state = str(state)
        self.country = str(country)
        self.favourites = str(favourites)
        self.awards = str(awards)
        self.ratings = str(ratings)
        self.status = 'available'
        self.confirmation_status = confirmation_status
        self.spare_dict = str(spare_dict)
        self.display_photo = display_photo


class Booking(ModelBase):
    __tablename__ = 'bookings_test'

    id = Column(Integer, primary_key=True)
    user_phone_number = Column(String(100000000))
    user_location = Column(String(100000000))
    artisan_id = Column(Integer)
    time_int = Column(Integer)
    ctime = Column(String(100000000))
    description = Column(String(100000000))
    description_photo_name = Column(String(100000000))
    status = Column(String(100000000))

    headers = ['id', 'user_phone_number', 'user_location', 'description',
               'description_photo_name', 'artisan_town', 'artisan_state',
               'artisan_country', 'artisan_name', 'artisan_phone',
               'artisan_skill']

    def head(self):
        head_ = {}

        for k in self.headers:

            if k.startswith('artisan_'):
                column_ = k.split('artisan_')[1]

                artisan = Artisan.query.filter(Artisan.id ==
                                               int(self.artisan_id)).first()
                head_[k] = getattr(artisan, column_)
                continue

            head_[k] = getattr(self, k)

        return head_

    def __init__(self, user_phone_number=None, user_location=None,
                 description_photo_name=None, description=None,
                 artisan_id=None):

        self.user_phone_number = user_phone_number
        self.user_location = user_location
        self.time_int = int(time.time())
        self.ctime = time.ctime()
        self.artisan_id = artisan_id
        self.description = description
        self.description_photo_name = description_photo_name
        self.status = 'open'


class User(ModelBase):
    __tablename__ = 'users_test'

    id = Column(Integer, primary_key=True)
    name = Column(Integer)
    phone = Column(Integer)
    city = Column(Integer)
    state = Column(String(100000000))
    call_history = Column(String(100000000))
    spare_dict = Column(String(100000000))

    def __init__(self, name=None, phone=None, city='', state='',
                 call_history='[]'):

        self.name = name
        self.phone = phone
        self.city = city
        self.state = state
        self.call_history = call_history


try:
    Artisan.__table__.create(engine)
    Booking.__table__.create(engine)
    User.__table__.create(engine)
except OperationalError:
    pass
