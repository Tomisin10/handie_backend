import os
import sys
sys.path.append('lib')

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


def get_env(env_key):
    return os.environ.get(env_key)


if not get_env('TEST_MODE'):
    engine = create_engine(
        'mysql+mysqldb://tomisin10:MoniaDev.@tomisin10.mysql.pythonanywhere-'
        'services.com/tomisin10$default?charset=utf8&'
        'use_unicode=0', pool_recycle=3600)
else:
    engine = create_engine('sqlite:///tmp/database.db',
                           convert_unicode=True,
                           echo=False)


metadata = MetaData(bind=engine)

# customers = Table('artisans', metadata,
#                   Column('id', Integer, primary_key=True),
#                   Column('name', String(1000000000)),
#                   Column('bio', String(1000000000)),
#                   Column('email', String(1000000000)),
#                   Column('status', String(1000000000)),
#                   Column('bookings', String(100000000)),
#                   Column('skill', String(1000000000)),
#                   Column('password', String(1000000000)),
#                   Column('phone', String(1000000000)),
#                   Column('street_address', String(1000000000)),
#                   Column('town', String(1000000000)),
#                   Column('state', String(1000000000)),
#                   Column('country', String(1000000000)),
#                   Column('favourites', String(1000000000)),
#                   Column('awards', String(1000000000)),
#                   Column('ratings', String(1000000000)),
#                   Column('status', String(1000000000)),
#                   Column('spare_dict', String(1000000000)),
#                   Column('confirmation_status', String(10000000000)))
#
# bookings = Table('bookings', metadata,
#                  Column('id', Integer, primary_key=True),
#                  Column('user_phone_number', String(10000000000)),
#                  Column('user_location', String(10000000000)),
#                  Column('artisan_id', Integer),
#                  Column('time_int', String(1000000000)),
#                  Column('ctime', String(1000000000)),
#                  Column('description', String(1000000000)),
#                  Column('description_photo_name', String(1000000000)),
#                  Column('media', String(10000000000)),
#                  Column('status', String(10000000000)))
#
# users = Table('users', metadata,
#               Column('id', Integer, primary_key=True),
#               Column('name', String(1000000000)),
#               Column('phone', String(1000000000)),
#               Column('city', String(1000000000)),
#               Column('state', String(1000000000)),
#               Column('call_history', String(1000000000)))

db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

metadata.create_all()  # bind=engine
for _t in metadata.tables:
    print('Table: %s (exists)' % _t)
