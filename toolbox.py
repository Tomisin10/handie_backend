from flask import current_app
from flask_mail import Message, Mail

handie_email = 'decave12357@gmail.com'  # 'handietechjo@gmail.com'


# TODO write make_booking_notification_mail method
def make_booking_notification_mail(**kwargs):
    # artisan_fullname, artisan_phone, artisan_skill,
    # user_fullname, user_location, user_phone_number, problem_description,
    # problem_media

    body = '''
{user_phone_number} in {user_location}
Is experiencing {problem_description}

And requested for
{artisan_fullname}
{artisan_phone}
{artisan_skill}
'''.format(**kwargs)

    mail = {'receiver_email_address': handie_email,
            'title': 'New Handie Booking',
            'body': body,
            'description_photo': kwargs['description_photo']}
    return mail


# TODO write send_booking_notification_mail method
def send_booking_notification_mail(mail, receiver_email_address, title, body,
                                   description_photo):
    msg = Message(
        subject=title, body=body,
        sender=("Handie Booking", handie_email),
        recipients=[receiver_email_address, "codelexis@gmail.com"])

    msg.attach("photo of problem.png", "image/png", description_photo)

    mail.send(msg)


def send_artisan_confirmation_status_change_notification():
    return
