import os
import time
from json import JSONEncoder, dumps as jsonify

from flask import Flask, Response
from flask import request, url_for, render_template, redirect, session
from flask_mail import Mail

import toolbox
from models import Artisan, Booking, User, db_session


__about__ = "Where you could find the best artisans."
__title__ = "Handie"

fb_app_id = ''


def env(env_key):
    return os.environ[env_key]


servers_ip, redis_port = '', ''

app = Flask(__name__)

app.config.update(
    CELERY_BROKER_URL='redis://%s:%s' % (servers_ip, redis_port),
    CELERY_RESULT_BACKEND='redis://%s:%s' % (servers_ip, redis_port),
    MAIL_SERVER='smtp.gmail.com', MAIL_PORT=587, MAIL_USE_SSL=False,
    MAIL_USE_TLS=True, MAIL_USERNAME=env('MAIL_USERNAME'),
    MAIL_PASSWORD=env('MAIL_PASSWORD')
)

mail = Mail(app)


def add_user_sample():
    customer = Artisan(
        name='Tomisin Abiodun', email='decave12357@gmail.com',
        password='gooder', address='9, Victor Ojelabi Avenue, Akinbo, Akute, '
                                   'Ogun State',
        phone='08107684553', interests=['Management', 'Programming'])
    db_session.add(customer)
    db_session.commit()


def edit_user_sample(email):
    user_file = Artisan.query.filter(Artisan.email == email).first()  # .update({User.foo: User.foo + 1})
    user_file.password = 'new password'
    db_session.commit()


def get_user_file(email):
    return Artisan.query.filter(Artisan.email == email).first()


@app.route('/')
def home():
    return 'Welcome to Handie'


@app.route('/artisan_signup', methods=['GET', 'POST'])
def artisan_signup():
    if request.method == 'POST':
        fullname = request.form.get('fullname')
        email = request.form.get('email')
        bio = request.form.get('bio')
        skill = request.form.get('skill')
        phone = request.form.get('phone')
        street_address = request.form.get('street_address')
        town = request.form.get('town')
        state = request.form.get('state')
        country = request.form.get('country')
        instagram_handle = request.form.get('instagram_handle')

        display_photo = request.files.get('display_photo')

        if display_photo:
            dp_fn = int(time.time())
            display_photo.save('static/blobs/%s_display_photo.png' % dp_fn)

        artisan = Artisan(name=fullname, bio=bio, email=email, skill=skill,
                          phone=phone, street_address=street_address,
                          town=town, state=state, country=country,
                          instagram_handle=instagram_handle,
                          display_photo='static/blobs/%s_display_photo.png' %
                                        dp_fn if display_photo else None)

        response = Response(
            jsonify({'status': 'successful', 'id': artisan.id})
        )

        db_session.add(artisan)

        try:
            db_session.commit()
        except:
            db_session.rollback()
            raise

        db_session.close()

        return response


@app.route('/user_signup', methods=['GET', 'POST'])
def user_signup():
    if request.method == 'POST':
        fullname = request.form.get('fullname')
        phone = request.form.get('phone')
        city = request.form.get('city')
        state = request.form.get('state')

        user = User(name=fullname, phone=phone, city=city, state=state)
        db_session.add(user)

        response = Response("{'status': 'successful', 'id': %s}" % user.id)

        try:
            db_session.commit()
        except:
            db_session.rollback()
            raise

        db_session.close()

        # response.headers['content_type'] = 'text/json'
        return response


@app.route('/rate_artisan')
def rate_artisan():
    artisan = request.args.get('artisan')
    rating = request.args.get('rating')

    artisan = Artisan.query.filter(Artisan.id == artisan)
    old_ratings = eval(artisan.ratings)
    old_ratings.append(rating)
    artisan.ratings = str(old_ratings)

    try:
        db_session.commit()
    except:
        db_session.rollback()
        raise

    db_session.close()


def save_image(image_data):
    img_fn = '%s' % int(time.time())
    if not os.path.exists('static/blobs'):
        os.mkdir('static/blobs')

    # image_data = image_data.decode()
    # open('blobs/%s.png' % img_fn, 'wb').write(image_data.file)
    image_data.save('static/blobs/%s.png' % img_fn)

    return img_fn


@app.route('/make_booking', methods=['GET', 'POST'])
def make_booking():
    if request.method == 'POST':
        # return "Successful", 200

        user_phone_number = request.form.get('user_phone_number')
        user_location = request.form.get('user_location')
        artisan_id = request.form.get('artisan_id')
        description = request.form.get('description')
        description_photo = request.files['description_photo']

        artisan_id = int(artisan_id)

        description_photo_name = save_image(description_photo)

        this_call = Booking(
            user_phone_number=user_phone_number, artisan_id=artisan_id,
            description=description, user_location=user_location,
            description_photo_name=description_photo_name)

        try:
            db_session.add(this_call)
            db_session.commit()
        except:
            db_session.rollback()
            raise

        db_session.close()

        # this_user = User.query.filter(User.id == user_id)
        # old_call_history = eval(this_user.call_history)
        # old_call_history.append(this_call.id)
        # this_user.call_history = str(old_call_history)
        # db_session.commit()

        try:
            this_artisan = Artisan.query.filter(
                Artisan.id == artisan_id).first()
        except:
            db_session.rollback()

        kwargs = {
            'artisan_fullname': this_artisan.name,
            'artisan_skill': this_artisan.skill,
            'artisan_phone': this_artisan.phone,
            'user_location': user_location,
            'user_phone_number': user_phone_number,
            'problem_description': description,
            'description_photo': open('static/blobs/%s.png' %
                                      description_photo_name,
                                      'rb').read()
        }

        mail_dict = toolbox.make_booking_notification_mail(**kwargs)
        toolbox.send_booking_notification_mail(mail, **mail_dict)

        return "Success", 200


@app.route('/get_skilled_artisan')
def get_skilled_artisan():
    skill = request.args.get('skill')

    try:
        artisans = Artisan.query.filter(
            Artisan.skill == skill,
            Artisan.confirmation_status == 'approved'
        ).all()
    except:
        db_session.rollback()
        db_session.close()

    list_of_artisans = []
    for person in artisans:
        list_of_artisans.append(Artisan.head(person))

    response = Response(str(list_of_artisans))
    response.headers['content_type'] = 'text/json'
    return response


@app.route('/change_artisan_confirmation_status/<int:artisan_id>/<new_status>')
def change_artisan_confirmation_status(artisan_id, new_status):
    artisan = Artisan.query.filter(Artisan.id == artisan_id).first()
    artisan.confirmation_status = new_status

    try:
        db_session.commit()
    except:
        db_session.rollback()

    db_session.close()

    toolbox.send_artisan_confirmation_status_change_notification()

    return "Successful", 200


@app.route('/get_pending_artisans/<int:starting_point>')
def get_pending_artisans(starting_point):
    try:
        artisans = Artisan.query.filter(
            Artisan.confirmation_status == 'pending').all()
    except:
        db_session.rollback()
        db_session.close()

    list_of_artisans = []
    for person in artisans:
        list_of_artisans.append(Artisan.head(person))

    list_of_artisans = list_of_artisans[starting_point:starting_point+10]

    json_list_of_artisans = JSONEncoder().encode(str(list_of_artisans))
    response = Response(json_list_of_artisans)
    response.headers['content_type'] = 'application/json'
    return response


@app.route('/get_existing_artisans/<int:starting_point>')
def get_existing_artisans(starting_point):
    try:
        artisans = Artisan.query.filter(
            Artisan.confirmation_status == 'approved').all()
    except:
        db_session.rollback()
        db_session.close()

    list_of_artisans = []
    for person in artisans:
        list_of_artisans.append(Artisan.head(person))

    list_of_artisans = list_of_artisans[starting_point:starting_point+10]

    json_list_of_artisans = JSONEncoder().encode(str(list_of_artisans))
    response = Response(json_list_of_artisans)
    response.headers['content_type'] = 'application/json'
    return response


@app.route('/get_artisan_head/<int:artisan_id>')
def get_artisan_head(artisan_id):
    try:
        artisans = Artisan.query.filter(Artisan.id == artisan_id).all()
    except:
        db_session.rollback()
        db_session.close()

    list_of_artisans = []
    for person in artisans:
        list_of_artisans.append(Artisan.head(person))

    json_list_of_artisans = JSONEncoder().encode(str(list_of_artisans))
    response = Response(json_list_of_artisans)
    response.headers['content_type'] = 'text/json'
    return response


@app.route('/get_bookings/<int:starting_point>')
def get_bookings(starting_point):
    try:
        artisans = Booking.query.filter(Booking.status == 'open').all()
    except:
        db_session.rollback()
        db_session.close()

    list_of_bookings = []
    for person in artisans:
        list_of_bookings.append(Booking.head(person))

    list_of_bookings.reverse()
    list_of_bookings = list_of_bookings[starting_point:starting_point+10]

    response = Response(str(list_of_bookings))
    response.headers['content_type'] = 'text/json'
    return response


@app.route('/get_artisan_photo/<int:id>')
def get_artisan_photo(id):
    try:
        artisan = Artisan.query.filter(Artisan.id == id).first()
    except:
        db_session.rollback()
        db_session.close()

    if artisan.instagram_handle:
        return ''

    else:
        photo_path = artisan.display_photo
        return "[%s]" % photo_path


if __name__ == '__main__':
    app.run()
